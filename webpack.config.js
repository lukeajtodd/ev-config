const merge = require('webpack-merge');
const siteConfig = require('./webpack_configs/webpack.config.site');
const adminConfig = require('./webpack_configs/webpack.config.admin');
const commonConfig = require('./webpack_configs/webpack.config.common');
const productionConfig = require('./webpack_configs/webpack.config.prod');

const IS_DEV = (process.env.NODE_ENV === 'development');
let common = commonConfig;

// If production
if (!IS_DEV) {
    common = merge(common, productionConfig);
}

module.exports = [
    merge(common, siteConfig), merge(common, adminConfig)
];