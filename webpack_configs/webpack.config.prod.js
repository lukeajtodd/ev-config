const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const ImageminPlugin = require('imagemin-webpack-plugin').default;
const ImageminMozjpeg = require('imagemin-mozjpeg');

module.exports = {
    optimization: {
        minimizer: [
            new UglifyJsPlugin({
                cache: true,
                parallel: true,
                sourceMap: true
            }),
            new OptimizeCSSAssetsPlugin({}),
            new ImageminPlugin({
                test: /\.(png)/,
                optipng: { optimizationLevel: 3 }
            }),
            new ImageminPlugin({
                test: /\.(svg)/,
                svgo: {
                    removeDoctype: true,
                    removeXMLProcInst: true,
                    removeComments: true,
                    removeMetadata: true,
                    removeEditorsNSData: true,
                    minifyStyles: true,
                    removeEmptyAttrs: true,
                    removeHiddenElems: true,
                    removeEmptyText: true,
                    removeEmptyContainers: true,
                    cleanupIDs: true,
                    removeRasterImages: true,
                    convertColors: true,
                    cleanupEnableBackground: true,
                    mergePaths: true,
                    removeUnknownsAndDefaults: true,
                    removeTitle: true,
                    removeDesc: true,
                    removeUnusedNS: true,
                    convertPathData: true,
                    convertTransform: true,
                    cleanupNumericValues: true,
                    cleanupListOfValues: true,
                    convertShapeToPath: true,
                    removeUselessDefs: true
                }
            }),
            new ImageminPlugin({
                test: /\.(jpe?g)/i,
                maxFileSize: 10000, // Only apply this one to files equal to or under 10kb
                plugins: [
                    ImageminMozjpeg({
                        quality: 80,
                        progressive: false
                    })
                ]
            }),
            new ImageminPlugin({
                test: /\.(jpe?g)/i,
                minFileSize: 10000, // Only apply this one to files over 10kb
                plugins: [
                    ImageminMozjpeg({
                        quality: 80,
                        progressive: true
                    })
                ]
            })
        ]
    }
};