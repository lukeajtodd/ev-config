const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const { InjectManifest } = require('workbox-webpack-plugin');
const DelWebpackPlugin = require('del-webpack-plugin');

// Is the current build a development build
const IS_DEV = (process.env.NODE_ENV === 'development');

/**
 * Webpack Configuration
 */
module.exports = {
    name: 'site',
    resolve: {
        modules: [
            path.resolve(__dirname, '../resources/assets/site/js'),
            path.resolve(__dirname, '../resources/assets/site/scss')
        ]
    },
    entry: {
        admin: [ path.resolve(__dirname, '../resources/assets/site/js/app.js'), path.resolve(__dirname, '../resources/assets/site/scss/app.scss') ]
    },
    output: {
        path: path.resolve(__dirname, '../app/webroot/'),
        filename: IS_DEV ? 'js/app.js' : 'js/app.min.js'
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: IS_DEV ? "css/app.css" : "css/app.min.css",
        }),
        new CopyWebpackPlugin([{
            context: path.resolve(__dirname, '../app/webroot/'),
            from: 'img',
            to: 'img-compiled'
        }]),
        new DelWebpackPlugin({
            info: true,
            include: ['precache-manifest.*.js']
        }),
        new InjectManifest({
            swSrc: path.resolve(__dirname, '../resources/assets/site/service-worker.js'),
            exclude: [/.*\.(?:png|jpg|jpeg|svg|gif)/]
        })
    ]
};