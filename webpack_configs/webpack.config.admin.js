const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

// Is the current build a development build
const IS_DEV = (process.env.NODE_ENV === 'development');

/**
 * Webpack Configuration
 */
module.exports = {
    name: 'admin',
    resolve: {
        modules: [
            path.resolve(__dirname, '../resources/assets/admin/js'),
            path.resolve(__dirname, '../resources/assets/admin/scss')
        ]
    },
    entry: {
        admin: [ path.resolve(__dirname, '../resources/assets/admin/js/admin.js'), path.resolve(__dirname, '../resources/assets/admin/scss/admin.scss') ]
    },
    output: {
        path: path.resolve(__dirname, '../app/View/Themed/Admin/webroot/'),
        filename: IS_DEV ? 'js/admin.js' : 'js/admin.min.js'
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: IS_DEV ? "css/admin.css" : "css/admin.min.css"
        }),
        new CopyWebpackPlugin([{
            context: path.resolve(__dirname, '../app/View/Themed/Admin/webroot/'),
            from: 'img',
            to: 'img-compiled'
        }])
    ]
};
