const path = require('path');
const webpack = require('webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
const devUrl = 'project-name.test';
let providePluginComponents = {
    $: "jquery",
    jQuery: "jquery",
    "window.jQuery": "jquery",
    Tether: "tether",
    "window.Tether": "tether",
    Popper: ['popper.js', 'default']
}

// Is the current build a development build
const IS_DEV = (process.env.NODE_ENV === 'development');

const publicPath = '/';

/**
 * Webpack Configuration
 */
module.exports = {
    resolve: {
        modules: [ 'node_modules' ]
    },
    stats: 'minimal',
    optimization: {
        splitChunks: {
            cacheGroups: {
                styles: {
                    name: 'styles',
                    test: /\.css$/,
                    chunks: 'all',
                    enforce: true
                }
            }
        }
    },
    plugins: [
        new webpack.DefinePlugin({
            IS_DEV: IS_DEV
        }),
        new webpack.ProvidePlugin(providePluginComponents),
        new BrowserSyncPlugin({
            host: devUrl,
            port: 3000,
            proxy: devUrl,
            open: false,
            files: [
                'app/**/*.php',
                'resources/views/**/*.php',
                'app/webroot/js/**/*.js',
                'app/webroot/css/**/*.css',
                'app/View/**/*.ctp'
            ]
        })
    ],
    module: {
        rules: [
            // BABEL
            {
                test: /\.jsx?$/,
                exclude: /(node_modules|bower_components)/,
                use: [
                    {
                        loader: 'babel-loader',
                        options: {
                            compact: true
                        }
                    }
                ]
            },

            // STYLES
            {
                test: /\.css$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    {
                        loader: 'css-loader',
                        options: {
                            importLoaders: 1,
                            sourceMap: IS_DEV
                        }
                    },
                    {
						loader: 'resolve-url-loader'
					},
                    {
                        loader: 'postcss-loader',
                        options: {
                            sourceMap: IS_DEV,
                            ident: 'postcss',
                            plugins: loader => [
                                require('postcss-cssnext')(),
                            ]
                        }
                    }
                ]
            },

            // CSS / SASS
            {
                test: /\.s[ac]ss/,
                use: [
                    MiniCssExtractPlugin.loader,
                    {
                        loader: 'css-loader',
                        options: {
                            sourceMap: IS_DEV
                        }
                    },
                    {
						loader: 'resolve-url-loader'
					},
                    {
                        loader: 'postcss-loader',
                        options: {
                            sourceMap: IS_DEV,
                            ident: 'postcss',
                            plugins: loader => [
                                require('postcss-cssnext')()
                            ]
                        }
                    },
                    {
                        loader: 'sass-loader',
                        options: {
                            sourceMap: true
                        }
                    }
                ]
            },

            {
				test: /\.html$/,
				use: [
                    'html-loader'
                ]
			},
	
			{
                test: /\.(png|jpe?g|gif)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: 'img-compiled/[name].[ext]?[hash]',
					        publicPath
                        }
                    }
                ]
			},
	
			{
                test: /\.(woff2?|ttf|eot|svg|otf)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: 'fonts-compiled/[name].[ext]',
                            publicPath
                        }
                    }
                ]
			},
	
			{
                test: /\.(cur|ani)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[name].[ext]?[hash]',
                            publicPath
                        }
                    }
                ]
			}
        ]
    }
};