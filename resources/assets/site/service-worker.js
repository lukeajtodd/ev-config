if (workbox) {
	workbox.precaching.precacheAndRoute(self.__precacheManifest || []);

	workbox.routing.registerRoute(
		new RegExp('.*\.js'),
		workbox.strategies.staleWhileRevalidate({
			cacheName: 'ev-js'
		})
	);

	workbox.routing.registerRoute(
		/.*\.css/,
		workbox.strategies.staleWhileRevalidate({
			cacheName: 'ev-css',
		})
	);

	workbox.routing.registerRoute(
		/img\/\.(?:png|jpg|jpeg|svg|gif)/,
		workbox.strategies.cacheFirst({
			cacheName: 'ev-images',
			plugins: [
				new workbox.expiration.Plugin({
					maxEntries: 20,
					maxAgeSeconds: 7 * 24 * 60 * 60,
				})
			],
		})
	);

	workbox.routing.registerRoute(
		/.*\.(?:eot|svg|ttf|woff|woff2)/,
		workbox.strategies.cacheFirst({
			cacheName: 'ev-fonts',
			plugins: [
				new workbox.expiration.Plugin({
					maxEntries: 8,
					maxAgeSeconds: 7 * 24 * 60 * 60,
				})
			],
		})
	);

}
